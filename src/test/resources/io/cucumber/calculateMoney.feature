Feature: Calculate Money

  Scenario Outline: Withdraw money
    Given Set default value 200
    When Withdraw cash <withdrawCash>
    Then Check current cash <expectedCash>
    And  Check remain money on account <remainingamount>

    Examples:
      | withdrawCash | expectedCash   | remainingamount |
      | 0            | 0              |      200        |
      | 50           | 50             |      150        |
      | 100          | 100            |      100        |
      | 150          | 150            |      50         |
      | 200          | 200            |      0          |
      | 500          | 0              |      200        |
