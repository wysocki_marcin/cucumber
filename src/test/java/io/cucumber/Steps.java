package io.cucumber;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.And;
import org.junit.Assert;

public class Steps {

    private int currentCash = 0;
    private int account = 0;
    private int defaultValue = 200;
    @Given("^Set default value (\\d+)$")
    public void setDefaultValue(int cash) {
        account = cash;
    }

    @When("^Withdraw cash (\\d+)$")
    public void withdrawCash(int cash) {
        if (account >= cash) {
            addToWallet(cash);
            substractFromAccount(cash);
        } else {
            System.out.println("You have not enough money to withdraw");
        }
    }

    private void addToWallet (int cash) {
        currentCash += cash;
    }

    private void substractFromAccount(int cash) {
        account -= cash;
    }

    @Then("^Check current cash (\\d+)$")
    public void checkCurrentCash(int cash) throws Throwable {
        String errorMessage = currentCash > cash ? "You have too much cash" : " You have not enough cash";
        if (currentCash != cash) {
            Assert.fail(errorMessage);
        }
    }

    @And("^Check remain money on account (\\d+)$")
    public void checkRemainMoneyOnAccount(int expectedMoney) {
        String errorMessage = account > expectedMoney ? "You have too much money on account" : " You have not enough money on account";
        if (account != expectedMoney) {
            Assert.fail(errorMessage);
        }
    }
}
